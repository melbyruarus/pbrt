//
//  planet.h
//  pbrt
//
//  Created by Melby Ruarus on 10/05/13.
//
//

#ifndef __pbrt__planet__
#define __pbrt__planet__

#include "materials/uber.h"

class PlanetMaterial : public UberMaterial {
public:
	PlanetMaterial(Reference<Texture<Spectrum> > kd,
				 Reference<Texture<Spectrum> > ks,
				 Reference<Texture<Spectrum> > kr,
				 Reference<Texture<Spectrum> > kt,
				 Reference<Texture<float> > rough,
				 Reference<Texture<Spectrum> > op,
				 Reference<Texture<float> > e,
				   Reference<Texture<float> > bump):
	UberMaterial(kd, ks, kr, kt, rough, op, e, bump) {}
};

PlanetMaterial *CreatePlanetMaterial(const Transform &xform,
									 const TextureParams &mp);

#endif /* defined(__pbrt__planet__) */
