//
//  planet.cpp
//  pbrt
//
//  Created by Melby Ruarus on 10/05/13.
//
//

#include "planet.h"

#include "paramset.h"
#include "textures/rampmap.h"
#include "textures/fbm.h"
#include "textures/multtex.h"
#include "textures/addtex.h"
#include "textures/constant.h"
#include "textures/mix.h"
#include "textures/wrinkled.h"

template <typename T>
class Perlin : public FBmTexture<T> {
	float _lower;
	float _upper;
public:
	Perlin(int octaves, float roughness, float scale, float lower, float upper, float angle=0):
	FBmTexture<T>(octaves, roughness, new IdentityMapping3D(Scale(scale, scale, scale) * Rotate(angle, Vector(1, 3, 4)))),
	_lower(lower), _upper(upper)
	{}
	
	T Evaluate(const DifferentialGeometry &dg) const {
		float val = FBmTexture<T>::Evaluate(dg);
		return ((val / 2.0) + 0.5) * (_upper - _lower) + _lower;
	}
};

template <typename T>
class Wrinkled : public WrinkledTexture<T> {
	float _lower;
	float _upper;
public:
	Wrinkled(int octaves, float roughness, float scale, float lower, float upper, float angle=0):
	WrinkledTexture<T>(octaves, roughness, new IdentityMapping3D(Scale(scale, scale, scale) * Rotate(angle, Vector(1, 3, 4)))),
	_lower(lower), _upper(upper)
	{}
	
	T Evaluate(const DifferentialGeometry &dg) const {
		float val = WrinkledTexture<T>::Evaluate(dg);
		return val/2.4 * (_upper - _lower) + _lower;
	}
};

class PolarTexture : public Texture<float> {
public:
    // UVTexture Public Methods
    PolarTexture() {
        mapping = new UVMapping2D();
    }
    ~PolarTexture() {
        delete mapping;
    }
										 
    float Evaluate(const DifferentialGeometry &dg) const {
        float s, t, dsdx, dtdx, dsdy, dtdy;
        mapping->Map(dg, &s, &t, &dsdx, &dtdx, &dsdy, &dtdy);
        return fabs(t*2-1);
    }
private:
    TextureMapping2D *mapping;
};

std::vector<std::pair<float, float> > thresholdRamp(float threshold, float bottom, float top) {
	std::vector<std::pair<float, float> > ramp;
	ramp.push_back(std::pair<float, float>(threshold - 0.000001, bottom));
	ramp.push_back(std::pair<float, float>(threshold - 0.000001, top));
	
	return ramp;
}

std::vector<std::pair<float, float> > rangeMapRamp(float bfrom, float tfrom, float bto, float tto) {
	std::vector<std::pair<float, float> > ramp;
	ramp.push_back(std::pair<float, float>(bfrom, bto));
	ramp.push_back(std::pair<float, float>(tfrom, tto));
	
	return ramp;
}

Texture<Spectrum> *mask(Texture<Spectrum> *tex, Texture<float> *by) {
	return new MixTexture<Spectrum>(new ConstantTexture<Spectrum>(Spectrum::FromRGB((const float []){0, 0, 0})), tex, by);
}

PlanetMaterial *CreatePlanetMaterial(const Transform &xform,
									 const TextureParams &mp)  {
	float _maxHeight = mp.FindFloat("heightscale", 0.2);
	float _snowLine = mp.FindFloat("snowline", 0.25);
	float _iceExtent = mp.FindFloat("iceextent", 0.8);
	float _oceanDepth = mp.FindFloat("oceandepth", 0.5);
	float _forestCover = mp.FindFloat("forestcover", 0.5);
	float _mountainHeight = mp.FindFloat("mountainheight", 0.6);
	float _terrainHeight = mp.FindFloat("terrainheight", 0.2);
	float _hillHeight = mp.FindFloat("hillheight", 0.2);
	float _seed = fmodf((mp.FindFloat("seed", 0)*123980), 1239);
	
	std::vector<std::pair<float, Spectrum> > heightColorMap;
	heightColorMap.push_back(std::pair<float, Spectrum>(0, Spectrum::FromRGB((const float []){0, 0, 0})));
	heightColorMap.push_back(std::pair<float, Spectrum>(1, Spectrum::FromRGB((const float []){1, 1, 1})));
	
	// Figure out what is land and what is sea + high-level structure
	Reference<Texture<float> > continentVariation;
	
	Reference<Texture<float> > heightImageTexture = mp.GetFloatTextureOrNull("height");
	if(heightImageTexture) {
		continentVariation = heightImageTexture;
	}
	else {
		Perlin<float> *fineVariation = new Perlin<float>(8, 0.5, 2, -0.1, 0.1, _seed);
		Perlin<float> *largeVariation = new Perlin<float>(10, 0.5, 0.5, 0, 1, 10+_seed);
		
		continentVariation = new AddTexture<float>(fineVariation, largeVariation);
	}
	
	Texture<float> *oceanMask = new RampMapTexture<float>(continentVariation, thresholdRamp(_oceanDepth, 1, 0));
	Texture<float> *landMask = new RampMapTexture<float>(continentVariation, thresholdRamp(_oceanDepth, 0, 1));
	
	// Calculate what the ocean looks like
	Texture<float> *oceanDepth = new RampMapTexture<float>(continentVariation, rangeMapRamp(0, _oceanDepth, 0, 1));
	
	std::vector<std::pair<float, Spectrum> > seaColorMap;
	seaColorMap.push_back(std::pair<float, Spectrum>(0, Spectrum::FromRGB((const float []){6/255.0, 13/255.0, 25/255.0})));
	seaColorMap.push_back(std::pair<float, Spectrum>(0.4, Spectrum::FromRGB((const float []){7/255.0, 15/255.0, 30/255.0})));
	seaColorMap.push_back(std::pair<float, Spectrum>(0.85, Spectrum::FromRGB((const float []){8/255.0, 28/255.0, 68/255.0})));
	seaColorMap.push_back(std::pair<float, Spectrum>(1, Spectrum::FromRGB((const float []){0.157, 0.408, 0.786})));
	
	Texture<Spectrum> *oceanColor = new RampMapTexture<Spectrum>(oceanDepth, seaColorMap);
	
	// Calculate the falloff for height near the edges of the continents
	Texture<float> *costalHeightFalloff = new RampMapTexture<float>(continentVariation, rangeMapRamp(_oceanDepth, _oceanDepth+0.1, 0, 1));
	
	// Calculate the mountains
	Texture<float> *mountains1 = new Wrinkled<float>(10, 0.6, 2, 0, 1, _seed);
	Texture<float> *mountains1Filtered = new RampMapTexture<float>(mountains1, rangeMapRamp(0.7, 1, 0, _mountainHeight));
	
	Texture<float> *height = new Perlin<float>(10, 0.6, 2, 0, _terrainHeight, 20+_seed);
	
	Texture<float> *hills = new Perlin<float>(8, 0.5, 7, 0, 1, _seed);
	Texture<float> *hillsFiltered = new RampMapTexture<float>(hills, rangeMapRamp(0.5, 1, 0, _hillHeight));
	
	Texture<float> *heightMap = new RampMapTexture<float>(new MultiplyTexture<float>(new AddTexture<float>(height, new AddTexture<float>(mountains1Filtered, hillsFiltered)), costalHeightFalloff), rangeMapRamp(0, 1, 0, _maxHeight));
	
	// Calculate what forests look like
	Texture<float> *forestNoise1 = new AddTexture<float>(new Perlin<float>(8, 0.8, 1, 0, 0.5, _seed), new Perlin<float>(10, 0.5, 1, 0, 0.5, _seed));
	
	std::vector<std::pair<float, Spectrum> > forestColorMap;
	forestColorMap.push_back(std::pair<float, Spectrum>(0.0, Spectrum::FromRGB((const float []){20/255.0, 31/255.0, 8/255.0})));
	forestColorMap.push_back(std::pair<float, Spectrum>(0.125, Spectrum::FromRGB((const float []){23/255.0, 43/255.0, 9/255.0})));
	forestColorMap.push_back(std::pair<float, Spectrum>(0.25, Spectrum::FromRGB((const float []){17/255.0, 35/255.0, 8/255.0})));
	forestColorMap.push_back(std::pair<float, Spectrum>(0.37, Spectrum::FromRGB((const float []){27/255.0, 35/255.0, 10/255.0})));
	forestColorMap.push_back(std::pair<float, Spectrum>(0.4, Spectrum::FromRGB((const float []){17/255.0, 50/255.0, 8/255.0})));
	forestColorMap.push_back(std::pair<float, Spectrum>(0.5, Spectrum::FromRGB((const float []){20/255.0, 31/255.0, 8/255.0})));
	forestColorMap.push_back(std::pair<float, Spectrum>(0.6, Spectrum::FromRGB((const float []){20/255.0, 31/255.0, 8/255.0})));
	forestColorMap.push_back(std::pair<float, Spectrum>(0.7, Spectrum::FromRGB((const float []){23/255.0, 43/255.0, 9/255.0})));
	forestColorMap.push_back(std::pair<float, Spectrum>(0.75, Spectrum::FromRGB((const float []){17/255.0, 35/255.0, 8/255.0})));
	forestColorMap.push_back(std::pair<float, Spectrum>(0.87, Spectrum::FromRGB((const float []){27/255.0, 35/255.0, 10/255.0})));
	forestColorMap.push_back(std::pair<float, Spectrum>(0.9, Spectrum::FromRGB((const float []){17/255.0, 50/255.0, 8/255.0})));
	forestColorMap.push_back(std::pair<float, Spectrum>(1.0, Spectrum::FromRGB((const float []){20/255.0, 31/255.0, 8/255.0})));
	
	Texture<Spectrum> *forestColor = new RampMapTexture<Spectrum>(forestNoise1, forestColorMap);
	
	// Calculate what deserts look like
	Texture<float> *desertNoise1 = new AddTexture<float>(new Perlin<float>(8, 0.9, 1, 0, 0.5, 1200+_seed), new Perlin<float>(10, 0.8, 1, 0, 0.5, 1230+_seed));
	
	std::vector<std::pair<float, Spectrum> > desertColorMap;
	desertColorMap.push_back(std::pair<float, Spectrum>(0.0, Spectrum::FromRGB((const float []){88/255.0, 70/255.0, 39/255.0})));
	desertColorMap.push_back(std::pair<float, Spectrum>(0.1, Spectrum::FromRGB((const float []){122/255.0, 95/255.0, 56/255.0})));
	desertColorMap.push_back(std::pair<float, Spectrum>(0.2, Spectrum::FromRGB((const float []){88/255.0, 48/255.0, 39/255.0})));
	desertColorMap.push_back(std::pair<float, Spectrum>(0.3, Spectrum::FromRGB((const float []){60/255.0, 50/255.0, 40/255.0})));
	desertColorMap.push_back(std::pair<float, Spectrum>(0.4, Spectrum::FromRGB((const float []){58/255.0, 48/255.0, 39/255.0})));
	desertColorMap.push_back(std::pair<float, Spectrum>(0.5, Spectrum::FromRGB((const float []){122/255.0, 95/255.0, 56/255.0})));
	desertColorMap.push_back(std::pair<float, Spectrum>(0.6, Spectrum::FromRGB((const float []){88/255.0, 70/255.0, 39/255.0})));
	desertColorMap.push_back(std::pair<float, Spectrum>(0.8, Spectrum::FromRGB((const float []){60/255.0, 50/255.0, 40/255.0})));
	desertColorMap.push_back(std::pair<float, Spectrum>(0.9, Spectrum::FromRGB((const float []){51/255.0, 48/255.0, 39/255.0})));
	desertColorMap.push_back(std::pair<float, Spectrum>(1.0, Spectrum::FromRGB((const float []){60/255.0, 50/255.0, 40/255.0})));
	
	
	Texture<Spectrum> *desertColor = new RampMapTexture<Spectrum>(desertNoise1, desertColorMap);
	
	// Calculate what ice looks like
	Texture<float> *iceProbability = new PolarTexture();
	Texture<float> *iceDensityNoise = new Perlin<float>(8, 0.7, 1, 0, 1, _seed);
	Texture<float> *coldness = new AddTexture<float>(new MultiplyTexture<float>(iceProbability, iceDensityNoise), new RampMapTexture<float>(heightMap, thresholdRamp(_maxHeight*_snowLine, 0, 1)));
	Texture<float> *iceMask = new RampMapTexture<float>(coldness, thresholdRamp(1-_iceExtent, 0, 1));
	Texture<float> *iceMaskInverse = new RampMapTexture<float>(coldness, thresholdRamp(1-_iceExtent, 1, 0));
	
	Texture<float> *iceNoise1 = new AddTexture<float>(new Perlin<float>(8, 0.9, 1, 0, 0.5, 2+_seed), new Perlin<float>(10, 0.8, 1, 0, 0.5, 55+_seed));
	
	std::vector<std::pair<float, Spectrum> > iceColorMap;
	iceColorMap.push_back(std::pair<float, Spectrum>(0.0, Spectrum::FromRGB((const float []){126/255.0, 174/255.0, 163/255.0})));
	iceColorMap.push_back(std::pair<float, Spectrum>(0.1, Spectrum::FromRGB((const float []){148/255.0, 174/255.0, 194/255.0})));
	iceColorMap.push_back(std::pair<float, Spectrum>(0.15, Spectrum::FromRGB((const float []){122/255.0, 134/255.0, 142/255.0})));
	iceColorMap.push_back(std::pair<float, Spectrum>(0.25, Spectrum::FromRGB((const float []){123/255.0, 136/255.0, 157/255.0})));
	iceColorMap.push_back(std::pair<float, Spectrum>(0.4, Spectrum::FromRGB((const float []){136/255.0, 164/255.0, 183/255.0})));
	iceColorMap.push_back(std::pair<float, Spectrum>(0.5, Spectrum::FromRGB((const float []){148/255.0, 174/255.0, 194/255.0})));
	iceColorMap.push_back(std::pair<float, Spectrum>(0.6, Spectrum::FromRGB((const float []){142/255.0, 184/255.0, 202/255.0})));
	iceColorMap.push_back(std::pair<float, Spectrum>(0.75, Spectrum::FromRGB((const float []){123/255.0, 166/255.0, 197/255.0})));
	iceColorMap.push_back(std::pair<float, Spectrum>(0.9, Spectrum::FromRGB((const float []){122/255.0, 134/255.0, 142/255.0})));
	iceColorMap.push_back(std::pair<float, Spectrum>(1, Spectrum::FromRGB((const float []){123/255.0, 136/255.0, 157/255.0})));
	
	Texture<Spectrum> *iceColor = new RampMapTexture<Spectrum>(iceNoise1, iceColorMap);
	
	// Composit the terain types to get the land color
	Texture<Spectrum> *landColor = new MixTexture<Spectrum>(forestColor, desertColor, new RampMapTexture<float>(new AddTexture<float>(new Perlin<float>(8, 0.5, 1, 0, 0.6, _seed), new Perlin<float>(8, 0.7, 1, 0, 0.4, _seed)), rangeMapRamp(_forestCover-0.1, _forestCover+0.1, 0, 1)));
		
    Texture<Spectrum> *Kd = new AddTexture<Spectrum>(mask(new AddTexture<Spectrum>(mask(oceanColor, oceanMask), mask(landColor, landMask)), iceMaskInverse), mask(iceColor, iceMask));
	
    Reference<Texture<Spectrum> > Ks = mask(mask(new ConstantTexture<Spectrum>(Spectrum::FromRGB((const float[]){1, 1, 1})), oceanMask), iceMaskInverse);
    Reference<Texture<Spectrum> > Kr = mp.GetSpectrumTexture("Kr", Spectrum(0.f));
    Reference<Texture<Spectrum> > Kt = mp.GetSpectrumTexture("Kt", Spectrum(0.f));
    Reference<Texture<float> > roughness = mp.GetFloatTexture("roughness", 0.005f);
    Reference<Texture<float> > eta = mp.GetFloatTexture("index", 0.f);
    Reference<Texture<Spectrum> > opacity = mp.GetSpectrumTexture("opacity", 1.f);
	
    Reference<Texture<float> > bumpMap = heightMap;
	
    return new PlanetMaterial(Kd, Ks, Kr, Kt, roughness, opacity, eta, bumpMap);
}