//
//  rampmap.h
//  pbrt
//
//  Created by Melby Ruarus on 10/05/13.
//
//

#ifndef __pbrt__rampmap__
#define __pbrt__rampmap__

#include "texture.h"
#include <vector>

template <typename T>
class RampMapTexture : public Texture<T> {
	std::vector<std::pair<float, T> > _colorPoints;
	Reference<Texture<float> > _baseTexture;
public:
	RampMapTexture(Reference<Texture<float> > baseTexture, std::vector<std::pair<float, T> > colorPoints):
	 _colorPoints(colorPoints), _baseTexture(baseTexture) {}
	
	virtual T Evaluate(const DifferentialGeometry &dg) const {
		float val = _baseTexture->Evaluate(dg);
		const T *last = &(_colorPoints[0].second);
		float lastPos = _colorPoints[0].first;
		if(val < lastPos) {
			return *last;
		}
		
		for(size_t n=1;n<_colorPoints.size();n++) {
			float currentPosition = _colorPoints[n].first;
			if(val < currentPosition) {
				float interp = (val - lastPos) / (currentPosition - lastPos);
				return *last * (1-interp) + _colorPoints[n].second * (interp);
			}
			last = &(_colorPoints[n].second);
			lastPos = currentPosition;
		}
		
		return _colorPoints[_colorPoints.size()-1].second;
	}
};

#endif /* defined(__pbrt__rampmap__) */
