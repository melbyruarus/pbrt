//
//  multtex.h
//  pbrt
//
//  Created by Melby Ruarus on 10/05/13.
//
//

#ifndef __pbrt__multtex__
#define __pbrt__multtex__

#include "texture.h"

template <typename T>
class MultiplyTexture : public Texture<T> {
	Reference<Texture<T> > _tex1;
	Reference<Texture<T> > _tex2;
public:
	MultiplyTexture(Reference<Texture<T> > tex1, Reference<Texture<T> > tex2):
	_tex1(tex1), _tex2(tex2) {}
	
	virtual T Evaluate(const DifferentialGeometry &dg) const {
		return _tex1->Evaluate(dg) * _tex2->Evaluate(dg);
	}
};

#endif /* defined(__pbrt__multtex__) */
