//
//  memoryimagefilm.h
//  pbrt
//
//  Created by Melby Ruarus on 20/05/13.
//
//

#ifndef __pbrt__memoryimagefilm__
#define __pbrt__memoryimagefilm__

#include "film/image.h"

class MemoryImageFilm : public ImageFilm {
public:
	MemoryImageFilm(int xres, int yres, Filter *filt, const float crop[4],
					const string &filename, bool openWindow):
	ImageFilm(xres, yres, filt, crop, filename, openWindow) {}
	
	float *rgb;
	
	virtual void WriteImage(float splatScale) {
		int nPix = xPixelCount * yPixelCount;
		rgb = new float[3*nPix];
		int offset = 0;
		for (int y = 0; y < yPixelCount; ++y) {
			for (int x = 0; x < xPixelCount; ++x) {
				// Convert pixel XYZ color to RGB
				XYZToRGB((*pixels)(x, y).Lxyz, &rgb[3*offset]);
				
				// Normalize pixel with weight sum
				float weightSum = (*pixels)(x, y).weightSum;
				if (weightSum != 0.f) {
					float invWt = 1.f / weightSum;
					rgb[3*offset  ] = max(0.f, rgb[3*offset  ] * invWt);
					rgb[3*offset+1] = max(0.f, rgb[3*offset+1] * invWt);
					rgb[3*offset+2] = max(0.f, rgb[3*offset+2] * invWt);
				}
				
				// Add splat value at pixel
				float splatRGB[3];
				XYZToRGB((*pixels)(x, y).splatXYZ, splatRGB);
				rgb[3*offset  ] += splatScale * splatRGB[0];
				rgb[3*offset+1] += splatScale * splatRGB[1];
				rgb[3*offset+2] += splatScale * splatRGB[2];
				++offset;
			}
		}
	}
};

MemoryImageFilm *CreateMemoryImageFilm(const ParamSet &params, Filter *filter);

#endif /* defined(__pbrt__memoryimagefilm__) */
