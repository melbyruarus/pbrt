//
//  rgbimagefilm.h
//  pbrt
//
//  Created by Melby Ruarus on 20/05/13.
//
//

#ifndef __pbrt__rgbimagefilm__
#define __pbrt__rgbimagefilm__

#include "film/image.h"

class RGBImageFilm : public ImageFilm {
public:
	RGBImageFilm(int xres, int yres, Filter *filt, const float crop[4],
					const string &filename, bool openWindow):
	ImageFilm(xres, yres, filt, crop, filename, openWindow) {}
	
	void prepare() {
		int nPix = xPixelCount * yPixelCount;
		rgb = new float[3*nPix];
	}
	float *rgb;
	
	virtual void WriteImage(float splatScale);
};

RGBImageFilm *CreateRGBImageFilm(const ParamSet &params, Filter *filter);

#endif /* defined(__pbrt__rgbimagefilm__) */
