//
//  materials.h
//  pbrt
//
//  Created by Melby Ruarus on 20/05/13.
//
//

#ifndef __pbrt__materials__
#define __pbrt__materials__

#include "../materials/uber.h"
#include "../materials/matte.h"
#include "../materials/glass.h"
#include "../materials/metal.h"
#include "renderer.h"
#include "scene.h"

class DifferentialMaterial {
public:
	void print(int t) { printf("%i\n", t); }
};

class DifferentialUberMaterial : public UberMaterial, public DifferentialMaterial {
public:
	DifferentialUberMaterial(Reference<Texture<Spectrum> > kd,
							 Reference<Texture<Spectrum> > ks,
							 Reference<Texture<Spectrum> > kr,
							 Reference<Texture<Spectrum> > kt,
							 Reference<Texture<float> > rough,
							 Reference<Texture<Spectrum> > op,
							 Reference<Texture<float> > e,
							 Reference<Texture<float> > bump):
	UberMaterial(kd, ks, kr, kt, rough, op, e, bump) {}
};

class DifferentialMatteMaterial : public MatteMaterial, public DifferentialMaterial {
public:
	DifferentialMatteMaterial(Reference<Texture<Spectrum> > kd,
							  Reference<Texture<float> > sig,
							  Reference<Texture<float> > bump):
	MatteMaterial(kd, sig, bump) {}
};

class DifferentialGlassMaterial : public GlassMaterial, public DifferentialMaterial {
public:
	DifferentialGlassMaterial(Reference<Texture<Spectrum> > r, Reference<Texture<Spectrum> > t,
							  Reference<Texture<float> > i, Reference<Texture<float> > bump):
	GlassMaterial(r, t, i, bump) {}
};

class DifferentialMetalMaterial : public MetalMaterial, public DifferentialMaterial {
public:
	DifferentialMetalMaterial(Reference<Texture<Spectrum> > eta,
							  Reference<Texture<Spectrum> > k, Reference<Texture<float> > rough,
							  Reference<Texture<float> > bump):
	MetalMaterial(eta, k, rough, bump) {}
};

DifferentialUberMaterial *CreateDifferentialUberMaterial(const Transform &xform, const TextureParams &mp);
DifferentialGlassMaterial *CreateDifferentialGlassMaterial(const Transform &xform, const TextureParams &mp);
DifferentialMatteMaterial *CreateDifferentialMatteMaterial(const Transform &xform, const TextureParams &mp);
DifferentialMetalMaterial *CreateDifferentialMetalMaterial(const Transform &xform, const TextureParams &mp);

#endif /* defined(__pbrt__materials__) */
