//
//  maskrenderer.h
//  pbrt
//
//  Created by Melby Ruarus on 20/05/13.
//
//

#ifndef __pbrt__maskrenderer__
#define __pbrt__maskrenderer__

#include "renderers/samplerrenderer.h"
#include "spectrum.h"
#include "scene.h"
#include "core/volume.h"

class MaskRenderer : public SamplerRenderer {
public:
	MaskRenderer(Sampler *s, Camera *c, SurfaceIntegrator *si,
                    VolumeIntegrator *vi, bool visIds):
	SamplerRenderer(s, c, si, vi, visIds) {}
	
	Spectrum Li(const Scene *scene, const RayDifferential &ray,
				const Sample *sample, RNG &rng, MemoryArena &arena,
				Intersection *isect = NULL, Spectrum *T = NULL) const {
		return scene->IntersectP(ray) ? Spectrum(1.0) : (scene->volumeRegion ? scene->volumeRegion->tau(ray, 0.5, rng.RandomFloat()) : Spectrum(0.0));
	}
	
    Spectrum Transmittance(const Scene *scene, const RayDifferential &ray,
						   const Sample *sample, RNG &rng, MemoryArena &arena) const {
		return Spectrum(0.0);
	}
};

#endif /* defined(__pbrt__maskrenderer__) */
