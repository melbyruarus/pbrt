//
//  octreeaccel.h
//  pbrt
//
//  Created by Melby Ruarus on 14/05/13.
//
//

#ifndef __pbrt__Octreeaccel__
#define __pbrt__Octreeaccel__

#include "pbrt.h"
#include "primitive.h"

struct OctTreeNode {
	bool isLeaf;
	bool isEmpty;
	int id;
	
	union {
		struct OctTreeNode *(nodes[8]);
		struct {
			struct OctTreeNode *o;
			struct OctTreeNode *ox;
			struct OctTreeNode *oy;
			struct OctTreeNode *oz;
			struct OctTreeNode *oxy;
			struct OctTreeNode *oyz;
			struct OctTreeNode *ozx;
			struct OctTreeNode *oxyz;
		};
	};
	
	std::vector<uint32_t> leaves;
	BBox bounds;
	
	OctTreeNode(BBox _bounds) {
		bounds = _bounds;
		isLeaf = true;
		isEmpty = false;
		
		for(int n=0;n<8;n++) {
			nodes[n] = NULL;
		}
	}
	
	~OctTreeNode() {
		for(int n=0;n<8;n++) {
			delete nodes[n];
		}
	}
	
	void createChildren() {
		Point origin = bounds.pMin;
		float size = (bounds.pMax - bounds.pMin).x / 2.0;
		Vector tomax = Vector(size, size, size);
		Vector x = Vector(size, 0, 0);
		Vector y = Vector(0, size, 0);
		Vector z = Vector(0, 0, size);
		
		o =		new OctTreeNode(BBox(origin,					origin + tomax));
		ox =	new OctTreeNode(BBox(origin + x,				origin + x + tomax));
		oy =	new OctTreeNode(BBox(origin + y,				origin + y + tomax));
		oz =	new OctTreeNode(BBox(origin + z,				origin + z + tomax));
		oxy =	new OctTreeNode(BBox(origin + x + y,			origin + x + y + tomax));
		oyz =	new OctTreeNode(BBox(origin + y + z,			origin + y + z + tomax));
		ozx =	new OctTreeNode(BBox(origin + z + x,			origin + z + x + tomax));
		oxyz =	new OctTreeNode(BBox(origin + x + y + z,		origin + x + y + z + tomax));
	}
};

class OctreeAccel : public Aggregate {
private:
	BBox _bounds;
	struct OctTreeNode *_rootNode;
	std::vector<Reference<Primitive> > _primitives;
	std::vector<BBox> _primBounds;
	uint32_t _maxDepth;
	uint32_t _maxPrims;
	
	void insert(struct OctTreeNode *node, uint32_t primitiveId, uint32_t depth);
	bool recursiveIntersect(struct OctTreeNode *node, float &newTMax, const Ray &ray, Intersection *isect) const;
	bool fasttraversal(struct OctTreeNode *node, float &tmax, int &x, int &y, int &z, const Ray &ray, Intersection *isect) const;
public:
	OctreeAccel(const std::vector<Reference<Primitive> > &prims, int maxDepth, int maxPrims);
	~OctreeAccel();
	
	BBox WorldBound() const { return _bounds; }
    bool CanIntersect() const { return true; }
    bool Intersect(const Ray &ray, Intersection *isect) const;
    bool IntersectP(const Ray &ray) const;
};

OctreeAccel *CreateOctreeAccelerator(const std::vector<Reference<Primitive> > &prims,
									   const ParamSet &ps);

#endif /* defined(__pbrt__Octreeaccel__) */
