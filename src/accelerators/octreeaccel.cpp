//
//  octreeaccel.cpp
//  pbrt
//
//  Created by Melby Ruarus on 14/05/13.
//
//

#include "octreeaccel.h"

#include "paramset.h"
#include "intersection.h"

#define USE_FAST_TRAVERSAL 1

static const bool printDebug = false;

template <typename T>
void print(const T &b) {
	printf("<%f %f %f>", b.x, b.y, b.z);
}

void print(const BBox &b) {
	printf("<");
	print(b.pMin);
	printf(", %f>", b.pMax.x - b.pMin.x);
}

bool floatClose(float one, float two) {
	return abs(*((int *)&one) - *((int *)&two)) < 200;
}

void populateIDs(struct OctTreeNode *node, int &num) {
	node->id = num;
	if(!node->isLeaf) {
		for(int n=0;n<8;n++) {
			num++;
			populateIDs(node->nodes[n], num);
		}
	}
}

void debug(struct OctTreeNode *node, int indentation, bool dontDeep = false) {
	for(int i=0;i<indentation;i++) {
		printf("=");
	}
	
	printf("%i ", node->id);
	print(node->bounds);
	printf(" ");
	
	if(node->isLeaf) {
		printf("(");
		const char *space = "";
		for(uint32_t n=0;n<node->leaves.size();n++) {
			uint32_t i = node->leaves[n];
			printf("%s%i", space, i);
			space = ", ";
		}
		printf(")\n");
	}
	else {
		printf("\n");
		
		for(int n=0;n<8;n++) {
			if(dontDeep) {
				if(node->nodes[n]->isLeaf) {
					debug(node->nodes[n], indentation+1);
				}
				else {
					for(int i=0;i<indentation+1;i++) {
						printf("=");
					}
					printf("%i ", node->nodes[n]->id);
					print(node->nodes[n]->bounds);
					printf(" non-leaf\n");
				}
			}
			else {
				debug(node->nodes[n], indentation+1);
			}
		}
	}
}

OctreeAccel::OctreeAccel(const std::vector<Reference<Primitive> > &prims, int maxDepth, int maxPrims) {
	_maxDepth = maxDepth;
	_maxPrims = maxPrims;
	
	// Fully refine primitives
	for (uint32_t i = 0; i < prims.size(); ++i) {
		prims[i]->FullyRefine(_primitives);
	}
	
	// Compute bounds for tree construction
    _primBounds.reserve(_primitives.size());
    for (uint32_t i = 0; i < _primitives.size(); ++i) {
        BBox b = _primitives[i]->WorldBound();
        _bounds = Union(_bounds, b);
        _primBounds.push_back(b);
    }
	
	BBox boundingCube = _bounds;
	Vector diff = boundingCube.pMax - boundingCube.pMin;
	float size = std::max(diff.x, std::max(diff.y, diff.z));
	boundingCube.pMax = boundingCube.pMin + Point(size, size, size);
	_rootNode = new OctTreeNode(boundingCube);
	
	for (uint32_t i = 0; i < _primitives.size(); ++i) {
		insert(_rootNode, i, 0);
	}
	
//	int n = 0;
//	populateIDs(_rootNode, n);
//	debug(_rootNode, 0);
}

OctreeAccel::~OctreeAccel() {
	delete _rootNode;
}

void OctreeAccel::insert(struct OctTreeNode *node, uint32_t primitiveId, uint32_t depth) {
	if(node->isLeaf) {
		if(node->leaves.size() == _maxPrims && depth < _maxDepth) {
			node->isLeaf = false;
			std::vector<uint32_t> leaves = node->leaves;
			leaves.push_back(primitiveId);
			node->leaves.clear();

			node->createChildren();
						
			for(uint32_t n=0;n<leaves.size();n++) {
				uint32_t id = leaves[n];
				insert(node, id, depth);
			}
		}
		else {
			node->leaves.push_back(primitiveId);
		}
	}
	else {		
		for(int n=0;n<8;n++) {
			if(_primBounds[primitiveId].Overlaps(node->nodes[n]->bounds)) {
				insert(node->nodes[n], primitiveId, depth+1);
			}
		}
	}
}

int check(float closest, float boundary, float direction) {
	if(floatClose(closest, boundary)) {
		if(direction > 0) {
			return 1;
		}
		else if(direction < 0) {
			return -1;
		}
		else {
			return 0;
		}
	}
	else if(closest > boundary) {
		return 1;
	}
	else/* if(closest < boundary)*/ {
		return -1;
	}
}

bool OctreeAccel::fasttraversal(struct OctTreeNode *node, float &tmax, int &x, int &y, int &z, const Ray &ray, Intersection *isect) const {
	if(x < 0) {
		if(y < 0) {
			if(z < 0) {
				return recursiveIntersect(node->o, tmax, ray, isect);
			}
			else if(z > 0) {
				return recursiveIntersect(node->oz, tmax, ray, isect);
			}
			else {
				return recursiveIntersect(node->o, tmax, ray, isect) || recursiveIntersect(node->oz, tmax, ray, isect);
			}
		}
		else if(y > 0) {
			if(z < 0) {
				return recursiveIntersect(node->oy, tmax, ray, isect);
			}
			else if(z > 0) {
				return recursiveIntersect(node->oyz, tmax, ray, isect);
			}
			else {
				return recursiveIntersect(node->oy, tmax, ray, isect) || recursiveIntersect(node->oyz, tmax, ray, isect);
			}
		}
		else {
			if(z < 0) {
				return recursiveIntersect(node->oy, tmax, ray, isect) || recursiveIntersect(node->o, tmax, ray, isect);
			}
			else if(z > 0) {
				return recursiveIntersect(node->oyz, tmax, ray, isect) || recursiveIntersect(node->oz, tmax, ray, isect);
			}
			else {
				return recursiveIntersect(node->oy, tmax, ray, isect) || recursiveIntersect(node->oyz, tmax, ray, isect) ||
					   recursiveIntersect(node->o, tmax, ray, isect) || recursiveIntersect(node->oz, tmax, ray, isect);
			}
		}
	}
	else if(x > 0) {
		if(y < 0) {
			if(z < 0) {
				return recursiveIntersect(node->ox, tmax, ray, isect);
			}
			else if(z > 0) {
				return recursiveIntersect(node->ozx, tmax, ray, isect);
			}
			else {
				return recursiveIntersect(node->ox, tmax, ray, isect) || recursiveIntersect(node->ozx, tmax, ray, isect);
			}
		}
		else if(y > 0) {
			if(z < 0) {
				return recursiveIntersect(node->oxy, tmax, ray, isect);
			}
			else if(z > 0) {
				return recursiveIntersect(node->oxyz, tmax, ray, isect);
			}
			else {
				return recursiveIntersect(node->oxy, tmax, ray, isect) || recursiveIntersect(node->oxyz, tmax, ray, isect);
			}
		}
		else {
			if(z < 0) {
				return recursiveIntersect(node->oxy, tmax, ray, isect) || recursiveIntersect(node->ox, tmax, ray, isect);
			}
			else if(z > 0) {
				return recursiveIntersect(node->oxyz, tmax, ray, isect) || recursiveIntersect(node->ozx, tmax, ray, isect);
			}
			else {
				return recursiveIntersect(node->oxy, tmax, ray, isect) || recursiveIntersect(node->oxyz, tmax, ray, isect) ||
					   recursiveIntersect(node->ox, tmax, ray, isect) || recursiveIntersect(node->ozx, tmax, ray, isect);
			}
		}
	}
	else {
		if(y < 0) {
			if(z < 0) {
				return recursiveIntersect(node->o, tmax, ray, isect) || recursiveIntersect(node->ox, tmax, ray, isect);
			}
			else if(z > 0) {
				return recursiveIntersect(node->oz, tmax, ray, isect) || recursiveIntersect(node->ozx, tmax, ray, isect);
			}
			else {
				return recursiveIntersect(node->o, tmax, ray, isect) || recursiveIntersect(node->oz, tmax, ray, isect) ||
					   recursiveIntersect(node->ox, tmax, ray, isect) || recursiveIntersect(node->ozx, tmax, ray, isect);
			}
		}
		else if(y > 0) {
			if(z < 0) {
				return recursiveIntersect(node->oy, tmax, ray, isect) || recursiveIntersect(node->oxy, tmax, ray, isect);
			}
			else if(z > 0) {
				return recursiveIntersect(node->oyz, tmax, ray, isect) || recursiveIntersect(node->oxyz, tmax, ray, isect);
			}
			else {
				return recursiveIntersect(node->oy, tmax, ray, isect) || recursiveIntersect(node->oyz, tmax, ray, isect) ||
				       recursiveIntersect(node->oxy, tmax, ray, isect) || recursiveIntersect(node->oxyz, tmax, ray, isect);
			}
		}
		else {
			if(z < 0) {
				return recursiveIntersect(node->oy, tmax, ray, isect) || recursiveIntersect(node->o, tmax, ray, isect) ||
						recursiveIntersect(node->oxy, tmax, ray, isect) || recursiveIntersect(node->ox, tmax, ray, isect);
			}
			else if(z > 0) {
				return recursiveIntersect(node->oyz, tmax, ray, isect) || recursiveIntersect(node->oz, tmax, ray, isect) ||
						recursiveIntersect(node->oxyz, tmax, ray, isect) || recursiveIntersect(node->ozx, tmax, ray, isect);
			}
			else {
				return recursiveIntersect(node->oy, tmax, ray, isect) || recursiveIntersect(node->oyz, tmax, ray, isect) ||
						recursiveIntersect(node->o, tmax, ray, isect) || recursiveIntersect(node->oz, tmax, ray, isect) ||
						recursiveIntersect(node->oxy, tmax, ray, isect) || recursiveIntersect(node->oxyz, tmax, ray, isect) ||
						recursiveIntersect(node->ox, tmax, ray, isect) || recursiveIntersect(node->ozx, tmax, ray, isect);
			}
		}
	}
}

bool OctreeAccel::recursiveIntersect(struct OctTreeNode *node, float &newTMax, const Ray &ray, Intersection *isect) const {
	float tmin, tmax;
	
	if(printDebug) {
		debug(node, 0, true);
	}
		
    if (!node->bounds.IntersectP(ray, &tmin, &tmax)) {
		return false;
	}
	
	newTMax = tmax;
	
	if(node->isLeaf) {
		bool hitSomething = false;
		
		for(uint32_t n=0;n<node->leaves.size();n++) {
			uint32_t i = node->leaves[n];
			if(isect) {
				hitSomething |= _primitives[i]->Intersect(ray, isect);
			}
			else {
				hitSomething |= _primitives[i]->IntersectP(ray);
			}
		}
		
		return hitSomething;
	}
	else {
#if USE_FAST_TRAVERSAL
		Point closest = ray.o + tmin * ray.d;
		float lastchildtmax = tmin;
		float childtmax;
		bool ret = false;
		while(true) {
			if (ray.maxt < tmin) return false;
			
			if(printDebug) {
				printf("tmin: %f\n", lastchildtmax);
				print(closest);
				printf("\n");
			}
			
			int x, y, z;
			
			x = check(closest.x, node->oxyz->bounds.pMin.x, ray.d.x);
			y = check(closest.y, node->oxyz->bounds.pMin.y, ray.d.y);
			z = check(closest.z, node->oxyz->bounds.pMin.z, ray.d.z);
			
			if(printDebug) {
				printf("%i %i %i\n", x, y, z);
			}
			
			ret |= fasttraversal(node, childtmax, x, y, z, ray, isect);
			if(ret && !isect) { // We are traversing front-to-back, so supposidly we can always return on the first intersection... but for some reason we can't
				return true;
			}
			else if(floatClose(tmax, childtmax)) { // we've come out the other side
				break;
			} else if(floatClose(lastchildtmax, childtmax)) { // we haven't moved
				break;
			}
			else {
				closest = ray.o + childtmax * ray.d;
				lastchildtmax = childtmax;
			}
		}
		return ret;
#else
		bool ret = false;
		float childtmax;
		for(int i=0;i<8;i++) {
			ret |= recursiveIntersect(node->nodes[i], childtmax, ray, isect);
		}
		return ret;
#endif
	}
}

bool OctreeAccel::Intersect(const Ray &ray, Intersection *isect) const {
	float tmin, tmax;
	_bounds.IntersectP(ray, &tmin, &tmax);
	bool ret = recursiveIntersect(_rootNode, tmax, ray, isect);
//	if(!ret) {
//		bool should = false;
//		for(int i=0;i<_primitives.size();i++) {
//			should = _primitives[i]->Intersect(ray, isect);
//			if(should) {
//				printf("\nShould have intersected: %i\nBounds: ", i);
//				print(_primitives[i]->WorldBound());
//				printf("\n");
//				_primitives[i]->WorldBound().IntersectP(ray, &tmin, &tmax);
//				printf("at tmin: %f\nWhich is: ", tmin);
//				print(ray.o + ray.d * tmin);
//				printf("\n");
//				break;
//			}
//		}
//		if(should) {
//			printDebug = true;
//			_bounds.IntersectP(ray, &tmin, &tmax);
//			recursiveIntersect(_rootNode, tmax, ray, isect);
//			printDebug = false;
//		}
//	}
	return ret;
}

bool OctreeAccel::IntersectP(const Ray &ray) const {
	float tmin, tmax;
	_bounds.IntersectP(ray, &tmin, &tmax);
	bool ret = recursiveIntersect(_rootNode, tmax, ray, NULL);
	return ret;
}

OctreeAccel *CreateOctreeAccelerator(const std::vector<Reference<Primitive> > &prims,
									   const ParamSet &ps) {
    return new OctreeAccel(prims, ps.FindOneInt("maxdepth", 7), ps.FindOneInt("maxprims", 8));
}